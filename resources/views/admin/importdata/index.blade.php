@extends('layouts.admin') 
@section('css')
  <link href="{{asset('assets/plugins/fileuploads/css/dropify.min.css' )}}" rel="stylesheet" type="text/css">
@endsection()
@section('content')
<div class="alert alert-danger">
  <b>Chú ý:</b> Khi thực hiện import dữ liệu. tất cả dữ liệu cũ sẽ bị xóa và sẽ không thể phục hồi lại.
</div>
<div class="alert alert-warning text-dark">
  <b>Cảnh báo: </b>Phải thực hết tất cả các bước import (import tất cả các file dữ liệu yêu cầu) để hệ thống hoạt động đúng.
</div>
<div class="alert alert-info">
  Để tiến hành import dữ liệu hãy nhấn vào nút "Tiến hành Import" bên dưới
</div>
<div class="text-center">
  <a href="{{route('getMajor', ['importAll' => 'true'])}}" class="btn btn-primary">Tiến hành Import</a>  
</div>
@endsection

@section('scripts')
@include('inc.admin.dropifyjs')
<script src="{{asset('assets/js/change-alert.js')}}"></script>
@endsection
