@extends('layouts.admin') 
@section('css')
  <link href="{{asset('assets/plugins/fileuploads/css/dropify.min.css' )}}" rel="stylesheet" type="text/css">
@endsection()
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
        <form id="main-form" action="{{route('postClass')}}" enctype="multipart/form-data" method="POST">
          @csrf         
          {{-- Some require field for form --}}
          @include('inc.admin.form-require-field')

          <div class="form-group">
            <label>Danh sách các lớp:</label>
            <input name="class" type="file" class="dropify" data-height="300" data-allowed-file-extensions='["xlsx", "xls"]' />
            @if($errors->has('class'))
              <div class="text-danger">
                {{$errors->first('class')}}
              </div>
            @endif
          </div>
          <button id="btnSubmit" class="btn btn-primary">
            @if ($importAll)
              Tạo lại danh sách lớp và tới bước tiếp theo
            @else
              Import
            @endif
          </button>
          <a target="_blank" href="{{asset('files/T_Malop.xlsx')}}" class="btn btn-success">
            <i class="fa fa-download"></i> Download file mẫu</a>
        </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
@include('inc.admin.dropifyjs')
<script src="{{asset('assets/js/change-alert.js')}}"></script>
@endsection
