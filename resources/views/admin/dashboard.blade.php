@extends('layouts.admin') 

@section('content')
<section class="row">
  {{-- Section header --}}
  <div class="col-md-12">
    <h2>Quản trị hệ thống</h2>
    <hr>   
  </div>
  {{-- End section header --}}
  
  @component('components.card-widget')
    @slot('value')
      <a href="{{route('getRegisIndexWithStatus', 
        ['status'=>config('constants.print_status.OPEN')])
      }}" class="text-custom">{!!$countRegis!!}</a>  
    @endslot
    @slot('title')
      <a href="{{route('getRegisIndexWithStatus', 
      ['status'=>config('constants.print_status.OPEN')])
    }}" class="text-dark">Bản điểm cần in</a> 
    @endslot 
  @endcomponent

  @component('components.card-widget')
    @slot('value')
      <a href="{{route('getFeedBackIndexWithStatus', ['status'=>'false'])}}"
         class="text-pink">{{$numberFeedBack}}</a>
    @endslot
    @slot('title')
      <a href="{{route('getFeedBackIndexWithStatus', ['status'=>'false'])}}"
        class="text-dark">Phản hồi chưa xử lý</a>   
    @endslot
  @endcomponent

</section>
<section class="row">
  {{-- Section header --}}
  <div class="col-md-12">
    <h2>Import dữ liệu</h2>
    <hr>
  </div>
  {{-- End section header --}}
  
  @component('components.card-widget')
    @slot('value')
      <a href="{{route('getImportIndex')}}" class="text-custom">Tất cả</a>
    @endslot
    @slot('title')
      <a href="{{route('getImportIndex')}}" class="text-dark">Import lại toàn bộ dữ từ liệu</a>   
    @endslot
  @endcomponent

  @component('components.card-widget')
    @slot('value')
      <a href="{{route('getMajor')}}" class="text-pink">Ngành</a>
    @endslot
    @slot('title')
      <a href="{{route('getMajor')}}" class="text-dark">Import danh sách ngành</a>   
    @endslot
  @endcomponent

  @component('components.card-widget')
    @slot('value')
      <a href="{{route('getClass')}}" class="text-success">Lớp</a>
    @endslot
    @slot('title')
      <a href="{{route('getClass')}}" class="text-dark">Import danh sách lớp</a>   
    @endslot
  @endcomponent

  @component('components.card-widget')
    @slot('value')
      <a href="{{route('getStudent')}}" class="text-warning">Sinh Viên</a>
    @endslot
    @slot('title')
      <a href="{{route('getStudent')}}" class="text-dark">Import danh sách sinh viên</a>   
    @endslot
  @endcomponent

  @component('components.card-widget')
    @slot('value')
      <a href="{{route('getTerm')}}" class="text-success">Học Kỳ</a>
    @endslot
    @slot('title')
      <a href="{{route('getTerm')}}" class="text-dark">Import danh sách học kỳ</a>   
    @endslot
  @endcomponent

  @component('components.card-widget')
    @slot('value')
      <a href="{{route('getActivity')}}" class="text-custom">Hoạt Động</a>
    @endslot
    @slot('title')
      <a href="{{route('getActivity')}}" class="text-dark">Import danh sách hoạt động</a>   
    @endslot
  @endcomponent

  @component('components.card-widget')
    @slot('value')
      <a href="{{route('getActivityDetail')}}" class="text-info" style="font-size: 28px;">Chi Tiêt Hoạt Động</a>
    @endslot
    @slot('title')
      <a href="{{route('getActivityDetail')}}" class="text-dark">Import chi tiêt hoạt động</a>   
    @endslot
  @endcomponent   

</section>

@endsection
