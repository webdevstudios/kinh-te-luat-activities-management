@extends('layouts.admin') 
@section('content')
  <div class="row">
    <div class="col-md-6">
      <div class="card-box">
        <div class="form-group">
          <label for="">Username</label>
          <label class="form-control">{{Auth::user()->username}}</label>
        </div>
        <div class="form-group">
          <label for="">Họ tên:</label>
          <label class="form-control">{{Auth::user()->fullname}}</label>
        </div>
        <form action="{{route('postRestPassword')}}" method="POST">
          @csrf
          <div class="form-group">
            <label for="">Mật khẩu hiện tại</label>
            <input type="password" name="oldpassword" class="form-control">
            @if($errors->has('oldpassword'))	
              <span class="invalid-feedback d-block" role="alert">
                <strong>{{$errors->first('oldpassword')}}</strong>
              </span>
           @endif
  
          </div>
          <div class="form-group">
            <label for="">Mật khẩu mới</label>
            <input type="password" name="password" class="form-control">
            @if($errors->has('password'))	
              <span class="invalid-feedback d-block" role="alert">
                <strong>{{$errors->first('password')}}</strong>
              </span>
             @endif
          </div>
          <div class="form-group">
            <label for="">Nhập lại mật khẩu</label>
            <input type="password" name="password_confirmation" class="form-control">
            @if($errors->has('password_confirmation'))	
              <span class="invalid-feedback d-block" role="alert">
                <strong>{{$errors->first('password_confirmation')}}</strong>
              </span>
             @endif
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Đổi mật khẩu</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection