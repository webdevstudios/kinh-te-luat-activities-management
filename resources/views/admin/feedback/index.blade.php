@extends('layouts.admin') 

@section('content')

<div class="card-box">
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $err)
           <li>{{ $err }}</li>
        @endforeach
      </ul>
    </div>
    @endif
  <div class="btn-group">
    <a href="{{route('getFeedBackIndex')}}" 
      class="btn btn-{{$status === config('constants.feedback_status.ALL')
      ?'success' : 'primary'}}
    rounded-0">Tất Cả</a>
    <a href="{{route('getFeedBackIndexWithStatus', ['status'=>'false'])}}"
       class="btn btn-{{$status === config('constants.feedback_status.OPEN')
       ?'success' : 'primary'}} 
        rounded-0">Chờ xử lý</a>
    <a href="{{route('getFeedBackIndexWithStatus', ['status'=>'true'])}}"
       class="btn btn-{{$status === config('constants.feedback_status.CLOSE')
       ?'success' : 'primary'}} 
       rounded-0">Đã xử lý</a>
  </div>
  <div class="table-responsive">
      <table class="table table-bordered">
          <thead>
            <tr>
              <th>STT</th>
              <th>MSSV</th>
              <th>Ngày Tạo</th>
              <th>Trạng Thái</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody>
            @php $i = 0; @endphp
            @foreach ($feedbacks as $feedback)
              <tr>
                <td>{{++$i}}</td>
                <td>{{$feedback->student_id}}</td>
                <td>{{$feedback->created_at->format('d/m/Y - H:i:s')}}</td>
                <td>{{$feedback->status? 'Đã xử lý': 'Chờ xử lý'}}</td>
                <td>
                  @if ($feedback->status == false)
                    <form class="d-inline" action="{{route('postMarkDoneFeedBack')}}" method="POST">
                      @csrf
                        <input type="hidden" name="id" value="{{$feedback->id}}">
                        <button class="btn btn-success" type="submit">Xử lý</button>
                    </form>
                  @endif
                  <a href="{{route('getDetailPage', ['id' => $feedback->id])}}" 
                    class=" btn btn-info">Chi tiết</a>
                </td>
              </tr>
            @endforeach 
            @if ($i == 0)
                <tr>
                  <td colspan="5" class="text-center">Không có dữ liệu để hiển thị</td>
                </tr>
            @endif    
          </tbody>
      </table>
      {{$feedbacks->links()}}
  </div>
</div>
@endsection