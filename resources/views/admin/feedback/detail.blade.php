@extends('layouts.admin') 
@section('content')
<div class="card-box p-5">
  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
  <table class="table table-bordered">
      <tr>
        <td>Người tạo</td>
        <td>{{$student->first_name}}&nbsp;{{$student->last_name}}</td>
        <td>Ngày tạo</td>
        <td>{{$feedback->created_at->format('d/m/Y - H:i:s')}}</td>
      </tr>
      <tr>
        <td>MSSV</td>
        <td>{{$student->student_id}}&nbsp;{{$student->last_name}}</td>
        <td>Trạng thái</td>
        <td>{{$feedback->status? 'Đã xử lý': 'Chờ xử lý'}}</td>
      </tr>
  </table>
  <hr>
  <label for=""><b>Nội dung:</b></label>
  <div>
    {{$feedback->content}}
  </div>
  <div class="pt-4">
    @if ($feedback->status == false)
        <form class="d-inline" action="{{route('postMarkDoneFeedBack')}}" method="POST">
          @csrf
            <input type="hidden" name="id" value="{{$feedback->id}}">
            <button class="btn btn-success" type="submit">Đánh dấu đã xử lý</button>
        </form>
     @endif
     <a href="{{route('getFeedBackIndex')}}" class="btn btn-info">Qua về danh sách</a>
  </div>
</div>
@endsection