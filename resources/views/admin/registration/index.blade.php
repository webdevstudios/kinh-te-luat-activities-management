@extends('layouts.admin') 

@section('content')

<div class="card-box">
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $err)
           <li>{{ $err }}</li>
        @endforeach
      </ul>
    </div>
    @endif
  <div class="btn-group">
    <a href="{{route('getRegisIndex')}}" 
      class="btn btn-{{config('constants.print_status.ALL') == $status? 'success': 'primary'}} rounded-0">
      Tất Cả</a>
    <a href="{{route('getRegisIndexWithStatus', ['status'=>config('constants.print_status.OPEN')])}}"
       class="btn btn-{{config('constants.print_status.OPEN') == $status? 'success': 'primary'}} rounded-0">Chờ xử lý</a>
    
    <a href="{{route('getRegisIndexWithStatus', ['status'=>config('constants.print_status.DONE')])}}"
       class="btn btn-{{config('constants.print_status.DONE') == $status? 'success': 'primary'}} rounded-0">Đã xử lý</a>
   
       <a href="{{route('getRegisIndexWithStatus', ['status'=>config('constants.print_status.CANCEL')])}}"
       class="btn btn-{{config('constants.print_status.CANCEL') == $status? 'success': 'primary'}} rounded-0">Đã hủy</a>
  </div>
  <div class="table-responsive">
     <table class="table table-bordered">
       <thead>
         <tr>
            <th>STT</th>
            <th>MSSV</th>
            <th>Ngày tạo</th>         
            <th>Trạng thái</th>         
            <th>Thao tác</th>
         </tr>
       </thead>
       <tbody>
         @php $i = 0; @endphp
         @foreach ($registrations as $registration)
            <tr>
              <td>{{++$i}}</td>
              <td>{{ $registration->student_id }}</td>
              <td>{{ $registration->created_at->format('d/m/Y - H:i:s') }}</td>         
              <td>
                @if($registration->status == config('constants.print_status.OPEN'))
                  Chờ xác nhận
                @elseif($registration->status == config('constants.print_status.DONE'))
                  Đã xử lý
                @elseif($registration->status == config('constants.print_status.CANCEL'))
                  Đã hủy
                @endif
              </td>  

              <td>
                @if($registration->status == config('constants.print_status.OPEN'))
                  <form class="d-inline" action="{{route('postSetStatusToDone')}}" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{$registration->id}}">
                    <button href="#" class="btn btn-success">Đánh dấu đã in</button>
                  </form>
                @endif
              </td>  

            </tr>
         @endforeach
         @if ($i == 0)
            <tr>
              <td class="text-center" colspan="5">
                Không có dữ liệu
              </td>
            </tr>
         @endif
       </tbody>
     </table>
     <div>{{$registrations->links()}}</div>
  </div>
</div>
@endsection