@extends('layouts.student')
@section('content')
  <h1>Lịch sử đăng ký in bảng điểm</h1>
  <div class="table-responsive">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th class="text-center">STT</th>
          <th class="text-center">Ngày Đăng Ký</th>
          <th class="text-center">Trạng Thái</th>
          <th class="text-center">Hủy</th>
        </tr>
      </thead>
      <tbody>
        @php
          $i = 0;
        @endphp
        @foreach ($printItems as $item)
          <tr>
            <td class="text-center">{{++$i}}</td>
            <td class="text-center">{{ $item->created_at->format('d/m/Y')}}</td>
            <td class="text-center">
              @if ($item->status == config('constants.print_status.OPEN'))
                <label class="badge badge-warning" style="font-size: 11px">Chờ In</label>  
              @elseif($item->status == config('constants.print_status.CANCEL'))
                <label class="badge badge-danger" style="font-size: 11px">Đã Hủy</label>  
              @else 
                <label class="badge badge-success" style="font-size: 11px">Đã In</label>                  
              @endif
            </td>
            <td class="text-center py-0">
              @if ($item->status == config('constants.print_status.OPEN'))
                <form method="POST" action="{{route('postPrintCancel')}}">
                  @csrf
                  <input type="hidden" name="id" value="{{$item->id}}">
                  <button type="submit" title="Hủy bỏ" class="btn btn-danger btn-block btn-sm mt-1">
                    Hủy
                  </button>
                </form>
              @endif
            </td>
          </tr> 
        @endforeach
        @if ($i==0)
          <tr>
            <td colspan="4">Bạn chưa đăng ký bảng điểm nào</td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>
@endsection