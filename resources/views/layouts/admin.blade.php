<!DOCTYPE html>
<html>
    
<!-- Mirrored from coderthemes.com/adminto/vertical_dark/typography.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Mar 2019 13:18:43 GMT -->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <title>UEL - Quản lý hoạt động xã hội | Admin</title>
        <!-- App css -->
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" />
        @yield('css')
        <script src="{{ asset('assets/js/modernizr.min.js') }}"></script>
    
    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            @include('inc.admin.top-bar')
            <!-- Top Bar End -->

            <!-- ========== Left Sidebar Start ========== -->
            @include('inc.admin.left-side-menu')
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                      @include('inc.message')
                      <div>@yield('content')</div>                     
                    </div> <!-- container -->
                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 - 2019 © Webdev Studios
                </footer>
            </div>

            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
        </div>
        <!-- END wrapper -->

        <!--Loading-->
        <div class="loadding-container">
         <img src="{{asset('assets/images/loading.gif')}}" alt="">
        </div>

        <!-- jQuery  -->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/js/popper.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/detect.js') }}"></script>
        <script src="{{ asset('assets/js/fastclick.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('assets/js/waves.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.scrollTo.min.js') }}"></script>
        
        <!-- App js -->
        <script src="{{ asset('assets/js/jquery.core.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.app.js') }}"></script>
        @yield('scripts')

        <script>
            $(function(){
                $('.loadding-container').css('display', 'none');
            });
            function showLoading() {
                $('.loadding-container').css('display', '');
            }
            $('#btnSubmit').on('click', function(e) {
                e.preventDefault();
                showLoading();
                $("#main-form").submit();
            });
        </script>
        
    </body>

<!-- Mirrored from coderthemes.com/adminto/vertical_dark/typography.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Mar 2019 13:18:43 GMT -->
</html>