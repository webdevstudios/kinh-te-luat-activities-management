<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>UEL - Bản Ghi Nhận Hoạt Động Công Tác Xã Hội</title>
  <link rel="icon" type="image/png" href="{{asset('assets/images/uelmini.png')}}" sizes="96x96">
  <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>
  <div id="top-border" style="background: url({{asset('assets/images/top.jpg')}}) repeat-x; height: 25px;">
  </div>
  <div class="container">
    <!--Header-->
    <header>
      <div class="row">
        <div class="col-12 mt-4">
          <div class="clearfix">
            <!-- Logo -->
            <div class="logo float-left">
              <a href="{{route('getStudentIndex')}}">
                <img  src="{{asset('assets/images/logo_uel.png')}}" 
                alt="lo go trường đh kinh tế luật">
              </a>
            </div> <!--./logo-->
            <!-- Logout -->
            <div class="logout-icon  float-right">
              @if (session('isLogin'))
                <form id="logout-form" action="{{route('postLogout')}}" method="POST">@csrf</form>
                <button id="btnLogout" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" 
                    type="submit" class="btn btnPrimary">
                  <span class="fa fa-user-circle-o"></span>&nbsp;Đăng xuất
                </button>
              @endif          
            </div><!-- ./Logout -->

          </div>
        </div>
        <div class="col-12 line">
          <img  style="width: 100%;" src="{{asset('assets/images/line_logo.png')}}" alt="bottom-line">
        </div>
      </div>
    </header>
    <!--End Header-->
    
    <div class="content">    
      <div class="row">
        <div class="col-md-3 mt-3">
            @include('inc.student.student-menu')
        </div>
        <div class="col-md-{{session('studentId')? '9' : '12'}}">
          @include('inc.message')
          @if ($errors->any())
            <div class="alert alert-danger mt-3">
              <ul class="m-0">
                @foreach ($errors->all() as $error)
                   <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          @yield('content')
        </div>         
      </div>
      @include('inc.student.student-login')
    </div>

        {{-- Confirm registration modal --}}
    @component('components.modal')
      @slot('id', 'modalConfirm')
      @slot('okLabel','Đăng ký')
      @slot('title','Đăng ký in bảng điểm')
      @slot('okEvent')
        document.getElementById('frmRegister').submit();
      @endslot
      {{-- Modal content --}}
      <form action="{{route('postRequest')}}" id="frmRegister" method="POST">@csrf</form>
      Bạn có chắc muốn đăng ký in bảng điểm không?
    @endcomponent

    {{-- Feedback modal --}}
    @component('components.modal')
      @slot('id', 'modalFeedback')
      @slot('okLabel','Phản hồi')
      @slot('title','Phản hồi thông tin cho người quản lý')
      @slot('okEvent')
        document.getElementById('frmFeedback').submit();
      @endslot
      <form action="{{route('postFeedBack')}}" method="post" id="frmFeedback">
        @csrf
        <div class="form-group">
          <label>Nhập thông tin phản hồi của bạn</label>
          <textarea name="content" class="form-control" rows="5"></textarea>
        </div>
      </form>
    @endcomponent

    <footer class="my-5">
      <div>Bản quyền &copy; 2019 thuộc Phòng Công tác Sinh viên Trường ĐH Kinh tế - Luật</div>
      <div><b>Được phát triển bởi</b>: Câu lạc bộ lập trình web 
        <a target="_blank" href="https://webdevstudios.org/">
          <b>Webdev Studios</b>
        </a>&nbsp;-&nbsp;UIT.
      </div>
    </footer>

    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
  </div>
</body>
</html>

