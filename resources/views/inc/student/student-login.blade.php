@if(!session('isLogin') && !session('studentId'))
  <div class="text-center p-5">
    <div class="card login-card" style="width: 15rem;">  
    <h1 style="border: none;">Đăng Nhập</h1>
    <div class="card-body">
      <a href="{{route('getStudentLogin')}}" class="btn btn-primary btn-sm btn-block">Đăng nhập</a>
    </div>
    </div>     
  </div>   
@endif