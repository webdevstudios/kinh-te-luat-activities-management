@if (session('studentId'))
  <div class="d-flex flex-column">
      {{-- Form đk bảng điểm --}}      
      <a href="#" data-toggle="modal" data-target="#modalConfirm" 
        class="btn menu-btn">
        &raquo;&nbsp;Đăng ký in bảng điểm
      </a>
      <a href="{{route('getPrint')}}" class="btn menu-btn">
        &raquo;&nbsp;Lịch sử đăng ký bảng điểm
      </a>
      <a href="#" data-toggle="modal" data-target="#modalFeedback" 
      class="btn menu-btn">&raquo;&nbsp;Phản hồi thông tin</a>
  </div>
@endif