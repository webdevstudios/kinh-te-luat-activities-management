<div>
	<div class="alert-area">
	@if(session('success') || isset($success))
		<div class="alert alert-success">
			{!! session('success') ? session('success') : $success !!}
		</div>
	@endif
	@if(session('error') || isset($error))
		<div class="alert alert-danger">
			{!! session('error')? session('error') : $error !!}
		</div>
	@endif	
	</div>
</div>
