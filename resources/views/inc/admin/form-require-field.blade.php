<div class="form-group">
  @if ($importAll == true)
    <div class="alert alert-warning">
      <b>Chú ý:</b>
      Chú ý phải thực hiện tất cả các bước import nếu không dữ hiệu 
      hệ thống sẽ bị xóa và hoạt động không đúng.
    </div>
    <input name="importAll" type="hidden" value="true">
    <input name="mode" type="hidden" value="{{config('constants.import_mode.add_new')['value']}}">
  @else
    <div id="altResetMode" class="alert alert-warning d-none">
      <b>Chú ý:</b> Khi bạn thực hiện xóa toàn bộ dữ liệu hiện tại và tạo mới, 
      toàn bộ dữ liệu của ngành và các bảng liên quan sẽ bị xóa 
      @if (isset($referTable))
        bao gồm: {{$referTable}}
      @endif.
    </div>
    <div id="altAddMode" class="alert alert-info">
      Dữ liệu thêm mới phải không được trùng với 
      dữ liệu hiện tại trong hệ thống nếu không quá trình import sẽ bị lỗi.
    </div>
    <input name="importAll" type="hidden" value="false">
    <label>Chế độ nhập liệu: </label>
    <select id="slImportMode" name="mode" class="form-control">
      @foreach (config('constants.import_mode') as $key => $item)
        <option value="{{$item['value'] }}">{{$item['diskplay']}}</option>
      @endforeach
    </select>
  @endif
</div>