<div class="topbar">

  <!-- LOGO -->
  <div class="topbar-left bg-dark">
      <a href="{{route('getDashboard')}}" class="logo">
        <img  src="{{asset('assets/images/logo.png')}}" alt="lo go trường đh kinh tế luật">
      </a>
  </div>

  <!-- Button mobile view to collapse sidebar menu -->
  <div class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
      <!-- Page title -->
      <ul class="nav navbar-nav list-inline navbar-left">
          <li class="list-inline-item">
              <button class="button-menu-mobile open-left">
                  <i class="mdi mdi-menu"></i>
              </button>
          </li>
          <li class="list-inline-item">
              <h4 class="page-title">{{$title}}</h4>
          </li>
      </ul>
      <nav class="navbar-custom mr-3">
        <div class="clearfix">
          <ul class="list-unstyled topbar-right-menu float-right mb-0">
            <li>
              <div class="btn-group dropleft">
                  <div class="waves-effect waves-light" data-toggle="dropdown"
                   aria-expanded="false"> <i class="fa fa-user-circle-o"></i>
                    {{Auth::user()->fullname}}
                    <span class="caret"></span>
                  </div>  
                  <div class="dropdown-menu" x-placement="bottom-start"
                  style="position: absolute;top: 20px; right: 0;">
                      <form id="logout-form" action="{{route('postAdminLogout')}}" method="POST">@csrf</form>
                      <a id="btnLogout" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();" class="dropdown-item">Đăng xuất
                      </a>
                      <a href="{{route('getUserProfile')}}" class="dropdown-item">Profile</a>
                  </div>
              </div>
           </li>
          </ul>
        </div>         
      </nav>
    </div><!-- end container -->
  </div><!-- end navbar -->
</div>