<div class="left side-menu">
  <div class="sidebar-inner slimscrollleft">
      <!--- Sidemenu -->
      <div id="sidebar-menu">
          <ul>
            <li class="text-muted menu-title">Menu</li>
            <li>
              <a href="{{route('getDashboard')}}" class="waves-effect">
                <i class="mdi mdi-view-dashboard"></i> 
                <span> Dashboard </span> 
              </a>
            </li>
            <li>
              <a href=" {{route('getFeedBackIndex') }}" class="waves-effect">
                <i class="mdi mdi-comment-multiple"></i> 
                <span> Phản hồi từ sinh viên </span>
               </a>
            </li>
            <li>
              <a href="{{route('getRegisIndex')}}" class="waves-effect">
                <i class="mdi mdi-bell-outline noti-icon"></i>
                <span> Yêu cầu in điểm </span>
              </a>
            </li>
            <li class="text-muted menu-title">Import dữ liệu</li>
            <li>
              <a href="{{route('getImportIndex')}}" class="waves-effect">
                <i class="mdi mdi-file-excel"></i>
                <span> Import lại toàn bộ dữ liệu</span> </a>
            </li>
            <li>
              <a href="{{route('getMajor')}}" class="waves-effect">
                <i class="mdi mdi-file-excel"></i> <span>Danh sách ngành học</span>
              </a>
            </li>
            <li>
              <a href="{{route('getClass')}}" class="waves-effect">
                <i class="mdi mdi-file-excel"></i> <span>Danh sách lớp</span>
              </a>
            </li>
            <li>
              <a href="{{route('getStudent')}}" class="waves-effect">
                <i class="mdi mdi-file-excel"></i>
                <span>Danh sách sinh viên</span> </a>
            </li>
            <li>
              <a href="{{route('getTerm')}}" class="waves-effect">
                <i class="mdi mdi-file-excel"></i><span>Danh sách học kỳ</span>
              </a>
            </li>
            <li>
              <a href="{{route('getActivity')}}" class="waves-effect">
                <i class="mdi mdi-file-excel"></i><span>Danh sách hoạt động</span>
              </a>
            </li>
            <li>
              <a href="{{route('getActivityDetail')}}" class="waves-effect">
                <i class="mdi mdi-file-excel"></i><span>Chi tiết hoạt động</span>
              </a>
            </li>
          </ul>
          <div class="clearfix"></div>
      </div>
      <!-- Sidebar -->
      <div class="clearfix"></div>

  </div>

</div>