<div class="col-xl-3 col-md-6">
  <div class="card-box widget-user">
    <div class="text-center">
    <h2 data-plugin="counterup">{{$value}}</h2>
      <h5>{{$title}}</h5>
    </div>
  </div>
</div>