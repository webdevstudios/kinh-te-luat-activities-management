<div class="modal fade"  data-backdrop="static"  id="{{$id}}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title">{{$title}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {!! $slot !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btnPrimary bg-secondary" data-dismiss="modal">Hủy bỏ</button>
      <button type="button" onclick="{{$okEvent}}" class="btn btnPrimary">{{$okLabel}}</button>
      </div>
    </div>
  </div>
</div>