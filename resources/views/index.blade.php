@extends('layouts.student')
@section('content')
  
  @if (session('studentId'))
    <h1>Bảng Ghi Nhận Hoạt Động Công Tác Xã Hội</h1>
      
    <!--Student info -->
    @if($student != null)
      <div class="table-responsive">
        <table class="table table-bordered my-4">
          <tr>
            <td class="fit">Họ và tên:</td>
            <td><b>{{$student->first_name}}&nbsp;{{$student->last_name}}</b></td>
            <td>MSSV:</td>
            <td><b>{{$student->student_id}}</b></td>
          </tr>
          <tr>
            <td  class="fit" >Ngày sinh:</td>
            <td>{{$student->dob}}</td>
            <td>Giới tính:</td>
            <td>{{$student->male == 0 ? 'NỮ' : 'NAM'}}</td>
          </tr>
          <tr>
            <td  class="fit">Ngành:</td>
            <td>{{$student->major_name}}</td>
            <td>Lớp:</td>
            <td>{{$student->class_name}}</td>
          </tr>
        </table>
      </div>
    @endif    
    <!-- End student info -->

    <div class="table-responsive">
      <!-- Activities -->
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="fit">STT</th>
            <th class="fit">Mã hoạt động</th>
            <th>Tên hoạt động</th>
            <th class="fit">Số ngày quy đổi</th>
            <th class="fit">Số ngày ghi nhận</th>
            <th>Ghi chú</th>
          </tr>
        </thead>
        <tbody>      
          <?php $numberDate = 0 ?>
          @if (sizeof($terms) > 0)    
            @foreach($terms as $term)
              <tr>
                <td colspan="6" class="td_courseyear">
                      NĂM HỌC: {{ $term->course_year}}&nbsp;-&nbsp;{{$term->name}}
                </td>
              </tr>
              <?php $i = 0 ?>                
              @foreach ($activities as $activity)
                <tr>                
                  @if($activity->term_code === $term->term_code)
                    <td>{{++$i}}</td>
                    <td>{{$activity->activity_code}}</td>
                    <td>{{$activity->name}}</td>
                    <td>{{$activity->numberOfDate}}</td>
                    <td>{{$activity->actual_date}}</td>
                    <td>{{$activity->note}}</td>
                    <?php  $numberDate += $activity->actual_date ?>
                  @endif
                </tr>
              @endforeach     
              @if ($i == 0)
                <tr>
                  <td colspan="5">Không có hoạt động nào đã tham gia trong học kỳ này</td>
                </tr>
              @endif         
            @endforeach    
          @else 
            <tr>
              <td colspan="5" class="text-center">
                Bạn chưa tham gia hoạt động xã hội nào
              </td>
            </tr>
          @endif              
        </tbody>
      </table>   
      <div id="divNumberDate">
        <i><b>Tổng số ngày công tác xã hội tích lũy: {{$numberDate}} ngày</b></i>
      </div>
      <!-- End Activities --> 
    </div>



  @endif
    
@endsection

