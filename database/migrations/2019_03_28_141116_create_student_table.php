<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('student_id', 20)->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->boolean('male');
            $table->char('dob', 10);
            $table->char('class_code', 20);
            $table->string('email');
            $table->foreign('class_code')
                  ->references('class_code')
                  ->on('class')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student');
    }
}
