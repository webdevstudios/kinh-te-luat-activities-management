<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrintRegistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('print_registration', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('student_id', 20);
            $table->integer('status');
            $table->timestamps();
            $table->foreign('student_id')
                  ->references('student_id')
                  ->on('student')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('print_registration');
    }
}
