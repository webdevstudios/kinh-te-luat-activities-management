<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('student_id', 20);
            $table->char('activity_code', 20);
            $table->integer('actual_date');
            $table->string('note')->nullable();
            $table->foreign('activity_code')
                  ->references('activity_code')
                  ->on('activity')
                  ->onDelete('cascade');
            $table->foreign('student_id')
                  ->references('student_id')
                  ->on('student')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_detail');
    }
}
