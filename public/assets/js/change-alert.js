$(function() {
  $('#slImportMode').on('change', function(e) {
    e.preventDefault;
    var value = parseInt($('#slImportMode').val());
    if(value == 1) {
      $("#altResetMode").removeClass('d-none');
      $("#altAddMode").addClass('d-none');
    } else {
      $("#altAddMode").removeClass('d-none');
      $("#altResetMode").addClass('d-none');
    }
  });
  $('#slImportMode').change();
});