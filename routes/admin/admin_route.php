<?php

  // Admin route
  Route::group(['prefix' => 'admin'], function() {

    Route::get('/login', [
      'as' => 'login',
      'uses' => 'Auth\LoginController@showLoginForm'
    ]);  
      
    Route::post('/login',[
      'as' => 'post.login',
      'uses' => 'Auth\LoginController@login'
    ]);

    // Require login.
    Route::group(['middleware'=>'auth'], function() {
      /**Admin routes which required login */
      require_once('auth/index.php');
      Route::group(['prefix' => 'import'], function() {
        require_once('auth/import.php');
      });
      /**Feed back */
      Route::group(['prefix' => 'feedback'], function() {
        require_once('auth/feedback.php');
      });
      Route::group(['prefix' => 'registration'],  function() {
        require_once('auth/registration.php');
      });
    });
  });
?>