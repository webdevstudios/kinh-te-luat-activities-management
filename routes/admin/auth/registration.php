<?php
  Route::get('/', [
    'as' => 'getRegisIndex',
    'uses' => 'Admin\PrintRegistrationController@index',
  ]);
  Route::post('/', [
    'as' => 'postSetStatusToDone',
    'uses' => 'Admin\PrintRegistrationController@setStatusToDone',
  ]);
  Route::get('/{status}', [
    'as' => 'getRegisIndexWithStatus',
    'uses' => 'Admin\PrintRegistrationController@index',
  ]); 
?>