<?php
  Route::get('/', [
    'as' => 'getDashboard',
    'uses' => 'Admin\HomeController@index'
  ]);
  Route::post('/logout', [
    'uses' => 'Auth\LoginController@logout',
    'as' => 'postAdminLogout'
  ]);
  
  /**User */
  Route::get('/profile', [
    'as' => 'getUserProfile',
    'uses' => 'Admin\UserController@profile'
  ]);
  Route::post('/profile', [
    'as' => 'postRestPassword',
    'uses' => 'Admin\UserController@resetPassword'
  ]);
?>