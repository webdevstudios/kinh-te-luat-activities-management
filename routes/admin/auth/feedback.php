<?php
  Route::get('/', [
    'uses' => 'Admin\FeedBackController@index',
    'as' => 'getFeedBackIndex'
  ]);

  Route::get('/detail/{id}', [
    'uses' => 'Admin\FeedBackController@showDetail',
    'as' => 'getDetailPage'
  ]);

  Route::get('/{status}', [
    'uses' => 'Admin\FeedBackController@index',
    'as' => 'getFeedBackIndexWithStatus'
  ]);

  Route::post('/', [
    'uses' => 'Admin\FeedBackController@markDoneFeedBack',
    'as' => 'postMarkDoneFeedBack'
  ]);
?>