<?php
  Route::get('/', [
    'as' => 'getImportIndex',
    'uses' => 'Admin\ImportDataController@index'
  ]);
  
  /**Major */
  Route::get('/major', [
    'as' => 'getMajor',
    'uses' => 'Admin\MajorController@getMajor'
  ]);
  Route::post('/major', [
    'as' => 'postMajor',
    'uses' => 'Admin\MajorController@postMajor'
  ]);
  
  /**Class */
  Route::get('/class', [
    'as' => 'getClass',
    'uses' => 'Admin\ClassController@getClass'
  ]);
  Route::post('/class', [
    'as' => 'postClass',
    'uses' => 'Admin\ClassController@postClass'
  ]);
  
  /**Student */
  Route::get('/student', [
    'as' => 'getStudent',
    'uses' => 'Admin\StudentController@getStudent'
  ]);
  Route::post('/student', [
    'as' => 'postStudent',
    'uses' => 'Admin\StudentController@postStudent'
  ]);
  
  /**Term */
  Route::get('/term', [
    'as' => 'getTerm',
    'uses' => 'Admin\TermController@getTerm'
  ]);
  Route::post('/term', [
    'as' => 'postTerm',
    'uses' => 'Admin\TermController@postTerm'
  ]);
  
  /**Activity */
  Route::get('/activity', [
    'as' => 'getActivity',
    'uses' => 'Admin\ActivityController@getActivity'
  ]);
  Route::post('/activity', [
    'as' => 'postActivity',
    'uses' => 'Admin\ActivityController@postActivity'
  ]);
  
  /**ActivityDetail */
  Route::get('/activitydetail', [
    'as' => 'getActivityDetail',
    'uses' => 'Admin\ActivityDetailController@getActivityDetail'
  ]);
  Route::post('/activitydetail', [
    'as' => 'postActivityDetail',
    'uses' => 'Admin\ActivityDetailController@postActivityDetail'
  ]);
?>