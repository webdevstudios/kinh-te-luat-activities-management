<?php

  /** =====The route can be used both when logged in and not logged in  ======== */
  Route::get('/', [
    'uses' => 'Student\StudentController@index',
    'as' => 'getStudentIndex' 
  ]);

  /** Route require not loggin */
  Route::group(['middleware' => 'checklogin:false'], function() {
    Route::get('auth/google', [
      'as' => 'getStudentLogin',
      'uses' => 'Student\StudentAuthController@redirectToProvider'
    ]);
    
    Route::get('/auth/google/callback', [
      'as' => 'callBackLogin',
      'uses' => 'Student\StudentAuthController@handleProviderCallback'
    ]);  
  });

  /*Routes require loggin*/
  Route::group(['middleware' => 'checklogin:true'], function() {  
    Route::post('/logout', [
      'uses' => 'Student\StudentController@logout',
      'as' => 'postLogout' 
    ]);

    Route::post('/phan-hoi', [
      'as' => 'postFeedBack',
      'uses' => 'Student\FeedBackController@postFeedBack'
    ]);

    Route::group(['prefix' => '/lich-su-ban-diem'], function() {
      Route::post('/cancel', [
        'as' => 'postPrintCancel',
        'uses' => 'Student\PrintRegistrationController@cancelPrint'
      ]);
      Route::post('/request', [
        'as' => 'postRequest',
        'uses' => 'Student\PrintRegistrationController@requestPrint'
      ]);
      Route::get('/', [
        'as' => 'getPrint',
        'uses' => 'Student\PrintRegistrationController@index'
      ]);
    });
  });

?>