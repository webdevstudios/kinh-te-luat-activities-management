<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailProvider extends Mailable
{
    use Queueable, SerializesModels;

    private $header;
    private $line;
    private $content;
    private $subjectStr;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($header, $line, $content, $subject)
    {
        $this->header = $header;
        $this->line = $line;
        $this->content = $content;
        $this->subjectStr = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.notify-feedback')->subject($this->subjectStr)
            ->with([ 'line' => $this->line,'content' => $this->content,'header' =>$this->header]);
    }
}
