<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityDetail extends Model
{
    protected $table = 'activity_detail';
    public $timestamps = false;
}
