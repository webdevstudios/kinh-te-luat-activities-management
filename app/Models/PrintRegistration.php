<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrintRegistration extends Model
{
    protected $table = 'print_registration';
}
