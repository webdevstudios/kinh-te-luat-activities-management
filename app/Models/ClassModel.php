<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassModel extends Model
{
    protected $table = 'class';
    
    protected $fillable = ['major_code', 'class_code', 'name'];
}
