<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FeedBackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return session('studentId');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => 'required|min:20',            
        ];
    }

    public function messages() {
        return [
            'content.required' => 'Bạn cần phải chú thích rõ thông tin nào đang hiển thị sai.
            để người quản lý có thể giúp bạn giải quyết.',
            'content.min' => 'Nội dung phản hồi quá ngắn, bạn nên chú thích rõ ràng hơn.'
        ];
    }
}
