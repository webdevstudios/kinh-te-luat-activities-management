<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class RestPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'oldpassword' => 'required',
            'password' => 'required|string|min:8|confirmed'
        ];
    }

    public function messages(){
        return [
            'oldpassword.required' => 'Trường này không được để trống',
            'password.required' => 'Trường này không được để trống',
            'password.min' => 'Mật khẩu phải ít nhất 8 ký tự',
            'password.confirmed' => 'Mật khẩu không khớp với mục xác thực',
            'password.string' => 'Sai định dạng cho mật khẩu',
        ];
    }

}
