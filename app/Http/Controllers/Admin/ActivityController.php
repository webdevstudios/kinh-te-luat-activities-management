<?php

namespace App\Http\Controllers\Admin;

use Excel;
use DB;
use Config;
use Exception;  
use App\Models\Activity;
use App\Models\ActivityDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivityController extends Controller
{
    /**
   * Delete data in major table and all table references to major table.
   */
    private function deleteReferDatabase() {
        DB::statement("SET foreign_key_checks=0");
        ActivityDetail::truncate();
        Activity::truncate();
        DB::statement("SET foreign_key_checks=1");
    }

    private function loadExcel($path) {
        $data = Excel::load($path)->get();

        if ($data->count()) {

        $firstRow = $data->first()->toArray();
        
        /**Validate file excel */
        if (!isset($firstRow['tenhoatdong']) 
        || !isset($firstRow['songayquydoi']) 
        || !isset($firstRow['mahocky'])) {
            return [];
        }

        foreach ($data as $key => $value) {
            $arr[] = [ 
                'name' => $value->tenhoatdong,   
                'activity_code' => $value->mahoatdong,
                'numberOfDate' => $value->songayquydoi,       
                'term_code' => $value->mahocky,       
            ];
        } 
            return $arr;
        } 

        return [];
    }

    public function getActivity(Request $request) {
        $title = 'Danh sách hoạt động';   
          
        if (!empty($request->importAll) 
        && $request->importAll == 'true') {

            return view('admin.importdata.activity', [
                'title' =>  $title, 
                'importAll' => true,
            ]);
        }
        
        return view('admin.importdata.activity', [
            'title' =>  $title,
            'importAll' => false,
            'referTable' => 'chi tiết hoạt động'
        ]);
    }

    public function postActivity(Request $request) {
        // Validate request.
        $request->validate([
            'activity' => 'required|file'
        ], [
            'activity.required' => 'Phải chọn một file để import dữ liệu',
            'activity.file'=> 'Đây không phải định dạng của file cần nhập'
        ]);

        $importAllTable = $request->importAll;
        DB::beginTransaction();
        try {
            $path = $request->file('activity')->getRealPath();
            $activities = $this->loadExcel($path);
            $data = Excel::load($path)->get();
            if (!empty($activities)){
                $createNewMode = Config::get('constants.import_mode.add_new.value');
                if ($request->mode !=  $createNewMode || $importAllTable  == 'true') {
                    $this->deleteReferDatabase();
                    Activity::insert($activities);
                } else {
                    Activity::insert($activities);
                }            
            } else  {
                return back()->with('error', 'File không đúng định dạng hoặc không có dữ liêu!'); 
            }
        } catch (Exception $e) {
            DB::rollback();   
            return back()->with('error', 'Xảy ra lỗi và không thể import dữ liệu. Hãy kiểm tra lại dữ liệu
                tất cả các cột dữ liệu không được trông'); 
        }
        DB::commit(); 
        
        $message = 'Đã tiến hành import dữ liệu cho hoạt động thành công.';
        if ($importAllTable == 'true') {
            return redirect()->route('getActivityDetail', ['importAll'  => 'true'])
            ->with('success', $message);
        } else {
            return back()->with('success', $message);
        }
    }    
}
