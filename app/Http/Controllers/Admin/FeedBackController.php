<?php

namespace App\Http\Controllers\Admin;

use App\Models\FeedBack;
use Config;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedBackController extends Controller
{
    public function index(Request $request) {
        if($request->status != null
        &&($request->status == 'true' ||  $request->status == 'false'))
        {
            $status = $request->status == 'true' 
                ? Config::get('constants.feedback_status.CLOSE')
                : Config::get('constants.feedback_status.OPEN');

            $feedbacks = FeedBack::orderBy('created_at', 'DESC')
                ->where('status' , '=' , $status)
                ->paginate(10);
        } else {
            $status = Config::get('constants.feedback_status.ALL');
            $feedbacks = FeedBack::orderBy('created_at', 'DESC')->paginate(10);
        }     

        return view('admin.feedback.index', [
            'title' => 'Các phản hồi từ sinh viên',
            'feedbacks' => $feedbacks,
            'status' => $status
        ]);
    }

    public function markDoneFeedBack(Request $request) {
        try {
            $request->validate(['id' => 'required'],
            ['id.required' => 'Trang của bạn yêu cầu không hợp lệ!']);
           
            $id=$request->id;
            $doneFeedBack = FeedBack::find($id);
           
            $doneFeedBack->status = true;
            $doneFeedBack->save();

            return back()->with(['success' => 'Đã đánh dấu phản hôi được xử lý thành công!']);
        } catch (\Exception $e) {
            return back()->with(['error' => 'Xảy ra lỗi, không thể đánh dấu phản hôi được xử lý!']);
        }
    }

    public function showDetail(Request $request){
        try {
            $id = $request->id;
            $feedback = Feedback::find($id);
            
            if($feedback) {
                $student = Student::where('student_id', '=', $feedback->student_id)->first();
            } else {
                return redirect()
                ->route('getDashboard')->with('error', 'Xảy ra lỗi, không thể load trang bạn yêu cầu');
            }
            
            return view('admin.feedback.detail', [
                'title' => 'Chi tiết phản hồi từ sinh viên',
                'feedback' => $feedback,
                'student' => $student
            ]);
        } catch (\Exception $e) {
            return redirect()->route('getDashboard')
            ->with('error', 'Xảy ra lỗi, không thể load trang bạn yêu cầu');
        }
    }
}
