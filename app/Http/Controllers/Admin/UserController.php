<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RestPasswordRequest;
use App\Models\User;
use Auth;
use Hash;
use Exception;

class UserController extends Controller
{
    public function profile() {
        return view('admin.user.profile', ['title' => 'Profile']);
    }

    public function resetPassword(RestPasswordRequest $request) {
        try {
            $username = Auth::user()->username;
            $password = $request->oldpassword;
            $currentPass =  Auth::user()->password;
            $newPassword = $request->password;

            if (Hash::check($password, $currentPass)) {
                $user_id = Auth::user()->id; 
                $saved_user = User::find($user_id);
                $saved_user->password = Hash::make($newPassword);
                $saved_user->save(); 

                return redirect()->back()->with('success', 'Đổi mật khẩu thành công!');
            } else {
                return redirect()->back()->with('error', 'Mật khẩu không chính xác!');
            }

        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Xảy ra lỗi, không thể đổi mật khẩu!');
        }
    }
}
