<?php

namespace App\Http\Controllers\Admin;

use Excel;
use DB;
use Config;
use Exception; 
use App\Models\Student; 
use App\Models\ActivityDetail; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    /**
     * Convert excel file to Student array.
     */
    private function loadExcel($path)
    {
      $data = Excel::load($path)->get();
     
      if($data->count()){
        $firstRow = $data->first()->toArray();
        if( !isset($firstRow['mssv']) || !isset($firstRow['email'])
        || !isset($firstRow['ho']) || !isset($firstRow['ten'])|| !isset($firstRow['phainam']) 
        || !isset($firstRow['ngaysinh']) || !isset($firstRow['malop']) ) {

            return [];
        }      

        foreach ($data as $key => $value) {
            $students[] = [ 
                'student_id' => $value->mssv, 
                'email' => $value->email,
                'first_name' => $value->ho,
                'last_name' => $value->ten,
                'male' => $value->phainam,
                'dob' => $value->ngaysinh,
                'class_code' => $value->malop            
            ];
        }       

        return $students;
      } else {
        return [];
      }
    }

    /**
     * Delete data in major table and all table references to major table.
     */
    private function deleteReferDatabase() {
        DB::statement("SET foreign_key_checks=0");
        ActivityDetail::truncate();
        Student::truncate();
        DB::statement("SET foreign_key_checks=1");
    }

    public function getStudent(Request $request) {
         $title = 'Danh sách sinh viên';
     
        if(!empty($request->importAll) && $request->importAll == 'true') {
            return view('admin.importdata.student', [
                'title' =>  $title, 
                'importAll' => true,
            ]);
        }

        return view('admin.importdata.student', [
            'title' =>  $title,
            'importAll' => false,
            'referTable' => 'chi tiết hoạt động'
        ]);
    }

    public function postStudent(Request $request) {
        // Validate request.
        $request->validate([
          'student' => 'required|file',
          'mode' => 'required',
          'importAll' => 'required'
        ],[
          'mode.required' => 'Thiếu chế độ nhập liệu',         
          'student.required' => 'Phải chọn một file để import dữ liệu',
          'student.file'=> 'Đây không phải định dạng của file cần nhập'
        ]);

        $importAllTable = $request->importAll;
        DB::beginTransaction();
        try {
            $path = $request->file('student')->getRealPath();
            $students = $this->loadExcel($path);
            if(!empty($students)){
                
                // Get import type.
                $createNewMode = Config::get('constants.import_mode.add_new.value');
               
                if($request->mode !=  $createNewMode || $importAllTable  == 'true') {
                    $this-> deleteReferDatabase();
                    Student::insert($students);
                } else {
                    Student::insert($students);
                }
            } else  {
                return back()->with('error', 'File không đúng định dạng hoặc không có dữ liêu!'); 
            }
        } catch (Exception $e) {
            DB::rollback();   
            return back()->with('error', 'Xảy ra lỗi và không thể import dữ liệu. 
            Hãy kiểm tra lại file của bạn tất cả các cột dữ liệu không được trông'); 
        }
        DB::commit();   

        $message = 'Đã tiến hành import dữ liệu danh sách sinh viên thành công.';
        if($importAllTable == 'true') {
            return redirect()->route('getTerm', ['importAll'  => 'true'])
            ->with('success', $message);
        } else {
            return back()->with('success', $message);
        }
    }
    
}
