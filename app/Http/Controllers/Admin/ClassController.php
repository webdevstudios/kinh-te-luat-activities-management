<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Config;
use Excel;
use Exception;
use App\Models\ClassModel;
use App\Models\Student;
use App\Models\Activity;
use App\Models\ActivityDetail;

class ClassController extends Controller
{
    /**
     * Convert excel file to array.
     */
    private function loadExcel($path) {
        $data = Excel::load($path)->get();     
        if($data->count()){
            $firstRow = $data->first()->toArray();
            // Return empty array if file invalid format.
            if(!isset($firstRow['malop']) || !isset($firstRow['manganh'])) {
                return [];
            }
            foreach ($data as $key => $value) {
                $arr[] = [ 'class_code' => $value->malop, 
                    'major_code' => $value->manganh,
                    'name' => $value->tenlop ];
            }

            return $arr;
        } else {
            return [];
        }     
    }

    /**
     * Delete data in class table and all table references to class table.
     */
    private function deleteReferDatabase() {
        DB::statement("SET foreign_key_checks=0");
        ClassModel::truncate();
        Student::truncate();
        ActivityDetail::truncate();
        DB::statement("SET foreign_key_checks=1");
    }

    public function getClass(Request $request) {
        $title = 'Danh sách Lơp';
        if(!empty($request->importAll) && $request->importAll == 'true') {
            return view('admin.importdata.class', [
                'title' => $title, 
                'importAll' => true,
            ]);
        }
        
        return view('admin.importdata.class', [
            'title' => $title, 
            'importAll' => false,
            'referTable' => 'danh sách sinh viên, chi tiết hoạt động'
        ]);
    }   

    /**
     * Import class form excel
     */
    public function postClass(Request $request) {
        // Validate request.
        $request->validate([
            'class' => 'required|file',
            'mode' => 'required',
            'importAll' => 'required'
        ],[
            'mode.required' => 'Thiếu chế độ nhập liệu',
            'class.required' => 'Phải chọn một file để import dữ liệu',
            'class.file'=> 'Đây không phải định dạng của file cần nhập'
        ]);

        $importAllTable = $request->importAll;

        DB::beginTransaction();

        try {
            $path = $request->file('class')->getRealPath();
            // Load array classes form excel.
            $classes =  $this->loadExcel($path);
            // dd($classes);
            if(!empty($classes)){
                $createNewMode = Config::get('constants.import_mode.add_new.value');
                if($request->mode !=  $createNewMode || $importAllTable == 'true') { 
                    // Reset table
                    $this->deleteReferDatabase();
                    ClassModel::insert($classes);
                } else { 
                    dd($createNewMode);
                    ClassModel::insert($classes);
                }
            } else  {            
                return back()->with('error', 'File không đúng định dạng hoặc không có dữ liêu trong file!'); 
            }
        } catch (Exception $e) {
            DB::rollback();   
            return back()->with('error', 'Xảy ra lỗi và không thể import dữ liệu.'); 
        }

        DB::commit();   

        $message = 'Đã tiến hành import dữ liệu danh sách lớp thành công.';
        if($importAllTable == 'true') {
            return redirect()->route('getStudent', ['importAll'  => 'true'])
            ->with('success', $message);
        } else {
            return back()->with('success', $message);
        }
    }    
}
