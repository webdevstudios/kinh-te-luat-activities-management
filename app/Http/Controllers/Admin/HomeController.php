<?php

namespace App\Http\Controllers\Admin;

use Config;
use App\Models\FeedBack;
use App\Models\PrintRegistration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index() {
        try {
            $openStatus = Config::get('constants.print_status.OPEN');
            $countRegis = PrintRegistration::where('status', '=', $openStatus)->count();
            $numberFeedBack = FeedBack::where('status', '=', false)->count();
        } catch(\Exception $e) {
            $numberFeedBack = 0;
        }
        return view('admin.dashboard', ['title' => 'Dashboard',
            'countRegis' => $countRegis,
            'numberFeedBack' => $numberFeedBack]);
    }    
}
