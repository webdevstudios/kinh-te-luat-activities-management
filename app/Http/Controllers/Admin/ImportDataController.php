<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImportDataController extends Controller
{
    public function index() {
        return view('admin.importdata.index', ['title' => 'Import dữ liệu']);
    }
}
