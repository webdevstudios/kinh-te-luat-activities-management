<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Excel;
use DB;
use Exception; 
use App\Models\ClassModel;
use Config;
use App\Models\Major;
use App\Models\Student;
use App\Models\Term;
use App\Models\Activity;
use App\Models\ActivityDetail;

class MajorController extends Controller
{
    /**
     * Delete all data in all table.
     */
    private function deleteDatabase() {
        DB::statement("SET foreign_key_checks=0");
        Major::truncate();
        ClassModel::truncate();
        Student::truncate();
        ActivityDetail::truncate();
        Activity::truncate();
        Term::truncate();
        DB::statement("SET foreign_key_checks=1");
    }
    
    /**
     * Delete data in major table and all table references to major table.
     */
    private function deleteReferDatabase() {
        DB::statement("SET foreign_key_checks=0");
        Major::truncate();
        ClassModel::truncate();
        Student::truncate();
        ActivityDetail::truncate();
        DB::statement("SET foreign_key_checks=1");
    }

    /**
     * Convert excel file to array.
     */
    private function loadExcel($path) {

        $data = Excel::load($path)->get();

        if($data->count()){
            $firstrow = $data->first()->toArray();
           
            /**Validate file excel */
            if(!isset($firstrow['manganh']) 
            || !isset($firstrow['tennganh'])) {
                return [];
            }  

            foreach ($data as $key => $value) {
                $arr[] = [
                    'major_code' => $value->manganh,
                    'name' => $value->tennganh
                ];
            }

            return $arr;
        } else {
            return [];
        }      
    }

    public function getMajor(Request $request) {

        // Reset database.
        if(!empty($request->importAll) 
        && $request->importAll == 'true') {

            return view('admin.importdata.major', [
                'title' => 'Danh sách Ngành', 
                'importAll' => true
            ]);
        }

         // Only import major table.
        return view('admin.importdata.major', [
            'title' => 'Danh sách Ngành', 
            'importAll' => false,
            'referTable' => 'danh sách lớp, danh sách sinh viên, chi tiết hoạt động'
        ]);
    }  

    public function postMajor(Request $request) {
        // Validate request.
        $request->validate([
            'major' => 'required|file',
            'mode' => 'required',
            'importAll' => 'required'
        ], [
            'mode.required' => 'Thiếu chế độ nhập liệu',
            'major.required' => 'Phải chọn một file để import dữ liệu',
            'major.file'=> 'Đây không phải định dạng của file cần nhập'
        ]);

        $importAllTable = $request->importAll;
        DB::beginTransaction();
            try {
                $path = $request->file('major')->getRealPath();
                $majors =  $this->loadExcel($path);
                if(!empty($majors)){
                    if($importAllTable  == 'true') { 
                    // Perform delete all data in database and import new data
                    // Full import.
                    $this->deleteDatabase();
                        Major::insert($majors);
                    } else {
                        $createNewMode = Config::get('constants.import_mode.add_new.value');
                        if($request->mode ==  $createNewMode ) { // Import mode = add new.
                            // Append data to current table.
                            Major::insert($majors);
                        } else {
                            // Delete all data in current table and insert new data.
                            $this->deleteReferDatabase();
                            Major::insert($majors);
                        }
                    }        
                } else  {
                    return back()->with('error', 'File nộp vào không đúng định dạng hoặc không có dữ liêu trong file!'); 
                }
            
            } catch (Exception $e) {
                DB::rollback();   
                return back()->with('error', 'Xảy ra lỗi và không thể import dữ liệu.'); 
            }

        DB::commit();   
        
        $message = 'Đã tiến hành import dữ liệu cho ngành thành công.';
        if($importAllTable == 'true') {
            return redirect()->route('getClass', ['importAll'  => 'true'])
                ->with('success', $message);
        } else {
            return back()->with('success', $message);
        }
    }    
}
