<?php

namespace App\Http\Controllers\Admin;

use Config;
use App\Models\PrintRegistration;
use App\Models\Student;
use Illuminate\Http\Request;
use Mail;
use DB;
use App\Mail\MailProvider;
use App\Http\Controllers\Controller;

class PrintRegistrationController extends Controller
{
    public function index(Request $request) {
        if($request->status != null
        && in_array($request->status, Config::get('constants.print_status'))) {
            $status = $request->status;
            $registrations = PrintRegistration::orderBy('created_at', 'DESC')
                ->where('status' , '=' , $status)->paginate(10);
        } else {
            $status = Config::get('constants.print_status.ALL');
            $registrations = PrintRegistration::orderBy('created_at', 'DESC')->paginate(10);
        }    

        return view('admin.registration.index', [
            'title' => 'Các yêu cầu in bảng điểm',
            'registrations' => $registrations,
            'status' => $status,
        ]);       
    }

    public function setStatusToDone(Request $request) {
        $request->validate([
            'id' => 'required'
        ], [
            'id.required' => 'Yêu cầu không hợp lệ, không thể đánh dấu đã in bảng điểm'
        ]);
        DB::beginTransaction();
        try {
            $id = $request->id;
            $doneObject = PrintRegistration::find($id);
            $doneObject->status = Config::get('constants.print_status.DONE');
            $doneObject->save();

            $studentId = $doneObject->student_id;
            $student = Student::where('student_id', '=', $studentId)->first();
           
            $subject = Config::get('mail.print_email.subject');
            $header =  Config::get('mail.print_email.mail_header');
            $line = "Chào bạn ".$student->first_name." ".$student->last_name."!<br>";

            $content = Config::get('mail.print_email.content');

            Mail::to($student->email)->send(new MailProvider($header, $line.$content, "", $subject));
            
            DB::commit(); 
            return back()->with('success', 'Cập nhật trạng thái thành công');
        } catch (\Exception $e) {
            DB::rollback(); 
            return back()->with('error', 'Xảy ra lỗi, không thể cập nhật trạng thái');
        }
    }
}
