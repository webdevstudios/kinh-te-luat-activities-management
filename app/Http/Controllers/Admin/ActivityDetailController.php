<?php

namespace App\Http\Controllers\Admin;

use Excel;
use DB;
use Config;
use Exception; 
use App\Models\ActivityDetail; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivityDetailController extends Controller
{
    /**Load excel file to array */
    private function loadExcel($path) {
        $data = Excel::load($path)->get();

        if ($data->count()){
            $firstRow = $data->first()->toArray();
          
            if (!isset($firstRow['mssv']) || !isset($firstRow['songaythucte'])|| !isset($firstRow['mahoatdong'])) {
                dd($firstRow);
                return [];
            }

            foreach ($data as $key => $value) {
                $arr[] = [ 
                    'student_id' => $value->mssv,
                    'actual_date' => $value->songaythucte,       
                    'activity_code' => $value->mahoatdong,    
                    'note' => $value->ghichu,      
                ];
            }
            return $arr;
        } else {
            return [];
        }
    }

    /** Get import activity detail from excel page */
    public function getActivityDetail(Request $request) {
        $title = 'Chi tiết hoạt động';

        if(!empty($request->importAll) && $request->importAll == 'true') {
            return view('admin.importdata.activity_detail', [
            'title' => $title, 
            'importAll' => true,
            ]);
        }

        return view('admin.importdata.activity_detail', [
            'title' => $title,
            'importAll' => false
        ]);
    }

    /**Import activity detail form excel */
    public function postActivityDetail(Request $request) {
        // Validate request.
        $request->validate([
            'activitydetail' => 'required|file',
            'mode' => 'required',
            'importAll' => 'required'
        ],[
            'mode.required' => 'Thiếu chế độ nhập liệu',          
            'activitydetail.required' => 'Phải chọn một file để import dữ liệu',
            'activitydetail.file'=> 'Đây không phải định dạng của file cần nhập'
        ]);

        $importAllTable = $request->importAll;

        DB::beginTransaction();

            try {
                $path = $request->file('activitydetail')->getRealPath();
                $activitydetails = $this->loadExcel($path);
                
                if (!empty($activitydetails)){
                    $createNewMode = Config::get('constants.import_mode.add_new.value');
                    if ($request->mode !=  $createNewMode || $importAllTable  == 'true') {
                        ActivityDetail::truncate();
                        ActivityDetail::insert($activitydetails);
                    } else {
                        ActivityDetail::insert($activitydetails);
                    }            
                } else  {
                    $err = 'File không đúng định dạng hoặc không có dữ liêu!';
                    return back()->with('error', $err); 
                }
            } catch (Exception $e) {
                DB::rollback();  // Rollback database when error occur

                $err = 'Xảy ra lỗi và không thể import dữ liệu.
                    Hãy kiểm tra lại dữ liệu và đảm bảo tất cả các cột dữ liệu không được trông';
                return back()->with('error', $err); 
            }

        DB::commit();  
        
        $message = 'Đã tiến hành import dữ liệu cho chi tiết hoạt động thành công.';
        if ($importAllTable == 'true') {
            return redirect()->route('getDashboard')
            ->with('success', $message);
        } else {
            return back()->with('success', $message);
        }
    }    
}
