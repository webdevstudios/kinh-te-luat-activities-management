<?php

namespace App\Http\Controllers\Admin;

use Excel;
use DB;
use Config;
// use Exception;  
use App\Models\Term;
use App\Models\ActivityDetail;
use App\Models\Activity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TermController extends Controller
{
  private function loadExcel($path)
  {
        $data = Excel::load($path)->get();
        if($data->count()){

            $firstrow = $data->first()->toArray();
            
            /**Validate file excel */
            if(!isset($firstrow['tenhocky'])
            || !isset($firstrow['namhoc'])
            || !isset($firstrow['mahocky'])) {
                return [];
            }
            
            foreach ($data as $key => $value) {
                $arr[] = [ 
                    'name' => $value->tenhocky,   
                    'course_year' => $value->namhoc,
                    'term_code' => $value->mahocky,       
                ];
            }
            return $arr;
        } else {
            return [];
        }
  }
      
  /**
   * Delete data in major table and all table references to major table.
   */
    private function deleteReferDatabase() {
        DB::statement("SET foreign_key_checks=0");
        ActivityDetail::truncate();
        Term::truncate();
        Activity::truncate();
        DB::statement("SET foreign_key_checks=1");
    }
    public function getTerm(Request $request) {
        $title = 'Học kỳ';     
        if(!empty($request->importAll) 
        && $request->importAll == 'true') {
            return view('admin.importdata.term', [
                'title' =>  $title, 
                'importAll' => true,
            ]);
        }
        
        return view('admin.importdata.term', [
            'title' =>  $title,
            'importAll' => false,
            'referTable' => 'danh sách hoạt động, chi tiết hoạt động'
        ]);
    }

  public function postTerm(Request $request) {
      // Validate request.
        $request->validate([
            'term' => 'required|file',
            'mode' => 'required',
            'importAll' => 'required'
        ],[
            'mode.required' => 'Thiếu chế độ nhập liệu',   
            'term.required' => 'Phải chọn một file để import dữ liệu',
            'term.file'=> 'Đây không phải định dạng của file cần nhập'
        ]);
        $importAllTable = $request->importAll;
        DB::beginTransaction();
        try {
            $path = $request->file('term')->getRealPath();
            $terms = $this->loadExcel($path);
           
            if(!empty($terms)){
                $createNewMode = Config::get('constants.import_mode.add_new.value');
                if($request->mode !=  $createNewMode || $importAllTable == 'true') { 
                    $this->deleteReferDatabase();
                    Term::insert($terms);
                } else {
                    Term::insert($terms);
                }
            } else  {
                return back()->with('error', 'File nộp vào không đúng định dạng hoặc không có dữ liêu!'); 
            }
        } catch (Exception $e) {
            DB::rollback(); 
              
            return back()->with('error', 'Xảy ra lỗi và không thể import dữ liệu.
            Hãy kiểm tra lại dữ liệu và đảm bảo tất cả các cột dữ liệu không được trông'); 
        }
        DB::commit();   
        $message = 'Đã tiến hành import dữ liệu cho học kỳ thành công.';
        if($importAllTable == 'true') {
            return redirect()->route('getActivity', ['importAll'  => 'true'])
            ->with('success', $message);
        } else {
            return back()->with('success', $message);
        }
  }
    
}
