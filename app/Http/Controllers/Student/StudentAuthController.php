<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Models\Student;
use Socialite;
use Exception; 
use Config;
use App\Http\Controllers\Controller;

class StudentAuthController extends Controller
{
    /**
     * Redirect the user to the Google authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        try {
            return Socialite::driver('google')
            ->with(['hd' => Config::get('services.google.hd')])->redirect();
        } catch (Exception $e) {
            return redirect()->route('getStudentIndex')->with('error', 'Xảy ra lỗi, xin hãy quay lại sau');
        }
    }

    /**
     * Obtain the user information from Google.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {      
        $user = Socialite::driver('google')->stateless()->user();

        $hd = Config::get('services.google.hd');
        $check = preg_match('/@'.$hd.'$/', $user->email);
        if(!$check) {
            return redirect()->route('getStudentIndex')
            ->with('error', '<b>Xảy ra lỗi: </b>Bạn phải đăng nhập bằng tài khoản email của trường.');
        }

        $student = Student::where('email', '=', $user->email)->first();
        session(['isLogin' => 'true']);
        session(['email' => $user->email]);
        
        if($student) {
            session(['studentId' => $student->student_id]);
            return redirect()->route('getStudentIndex');
        } else {
            return redirect()->route('getStudentIndex')
                ->with('error', 'Không có sinh viên nào khớp với tài khoản 
                của bạn, kiểm tra lại tài khoản của bạn!');
        }        
    }
}
