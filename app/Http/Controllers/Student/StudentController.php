<?php

namespace App\Http\Controllers\Student;

use App\Models\Student;
use DB;
use Illuminate\Http\Request;
use Exception; 
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    
    public function index() { 
        if(session('studentId')) {
            try {

                $studentId = session()->get('studentId');

                $student = DB::table('student')
                        ->join('class', 'class.class_code', '=', 'student.class_code')
                        ->join('major', 'major.major_code', '=', 'class.major_code')
                        ->where('student.student_id', '=', $studentId)
                        ->select('student.*', 'major.name as  major_name', 'class.name as class_name')                  
                        ->distinct()->get();
    
                $studentDAO = DB::table('student')
                            ->join('activity_detail', 'student.student_id', '=', 'activity_detail.student_id')
                            ->join('activity', 'activity_detail.activity_code', '=', 'activity.activity_code')
                            ->join('term', 'term.term_code', '=', 'activity.term_code')
                            ->where('student.student_id', '=', $studentId);
    
                $activities = $studentDAO
                            ->select('activity.*', 'activity_detail.actual_date', 'activity_detail.note')
                            ->distinct()->get();
                            
                $terms =  $studentDAO->select('term.*')->distinct()->get();;
    
                return view('index', [
                    'student' => sizeof($student) > 0 ? $student[0] : null,
                    'activities' => $activities,
                    'terms' => $terms
                ]);

            } catch(Exception $e) {
                return view('index', ['error' => "Hệ thống đang gặp sự cố, mong bạn quay lại sau!"]);
            }
        }
        if(session('isLogin')) {
            return view('index', ['error' => "Không có sinh viên nào khớp với tài khoản của bạn, kiểm tra lại tài khoản của bạn!"]);
        }

        return view('index');

    }

    public function logout() {
        session()->forget('studentId');
        session()->forget('isLogin');
        return redirect()->route('getStudentIndex');
    }
}
