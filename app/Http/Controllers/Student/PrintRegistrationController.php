<?php

namespace App\Http\Controllers\Student;

use Exception;
use Config;
use App\Models\PrintRegistration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrintRegistrationController extends Controller
{
    public function index() {
        try {
            $studentId = session()->get('studentId');
            $printItems = PrintRegistration::orderBy('created_at', 'DESC')
                ->where('student_id', '=', $studentId)->get(); 

            return view('print-history', ['printItems' => $printItems]);
        } catch (Exception $e) {
           return redirect()-route('getStudentIndex');
        }
    }

    public function cancelPrint(Request $request) {
        $request->validate([
            'id' => 'required'
          ], [
            'id.required' => 'Yêu cầu của bạn không thể thực hiện, xin hay tử lại sau!'
        ]);
        try {
            $id = $request->id;
            $studentId = session()->get('studentId');
            $printItem = PrintRegistration::find($id);

            if($studentId ==  $printItem->student_id
            && $printItem->status == Config::get('constants.print_status.OPEN')) {
                $printItem->status = Config::get('constants.print_status.CANCEL');
                $printItem->save();  
                return back()->with(['success' => 'Đã hủy yêu cầu in bản điểm thanh công.']);  
            }

            return back()->with(['success' => 'Bạn không được quyền hủy yêu cầu in bảng điểm này.']);              
        } catch (Exeption $e) {
            return back()->with(['error' => 'Xảy ra lỗi, không thể hủy yêu cầu in.']);
        }
    }

    public function requestPrint(Request $request) {
        try {
            $studentId = session()->get('studentId');
            $numberOpenRequest = PrintRegistration::where([
                ['student_id', '=', $studentId],
                ['status', '=', Config::get('constants.print_status.OPEN')]
            ])->count();
            if($numberOpenRequest > 0) {
                return back()->with(['error' => 'Bạn không thể đăng ký mới cho đến 
                    khi bảng điểm đang chờ in của bạn được in.']);
            }
            $newPrint = new PrintRegistration;
            $newPrint->student_id = $studentId;
            $newPrint->status = Config::get('constants.print_status.OPEN');
            $newPrint->save();

            return redirect()->route('getPrint');
        } catch (Exeption $e) {
            return back()->with(['error' => 'Xảy ra lỗi, không thể yêu cầu in bảng điểm.']);
        }
    }
}
