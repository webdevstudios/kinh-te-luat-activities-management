<?php

namespace App\Http\Controllers\Student;

use Config;
use Mail;
use DB;
use App\Http\Requests\FeedBackRequest;
use App\Models\FeedBack;
use App\Models\Student;
use App\Mail\MailProvider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{
    public function postFeedBack(FeedBackRequest $request) {
        DB::beginTransaction();
        try {

            if(!session('studentId')) {
                return redirect()->route('getStudentIndex');
            }
            $studentId = session()->get('studentId');
            $student = Student::where('student_id', '=',$studentId)->first();

            if($student) {

                $newFeedBack = new FeedBack;
                $newFeedBack->content = $request->content;
                $newFeedBack->status = false;
                $newFeedBack->student_id = $studentId;
                $newFeedBack->save();

                $subject = Config::get('mail.feedback_mail.subject');
                $header =  Config::get('mail.feedback_mail.mail_header');
                $line = "Hệ thống nhận được phản hồi từ <b>".$student->first_name." ".$student->last_name 
                ." - MSSV: ".$student->student_id."</b> với nội dung như sau:";

                $content = $newFeedBack->content;

                $mails = explode('|', Config::get('mail.feedback_mail.receiver_address'));
                Mail::to($mails)->send(new MailProvider($header, $line, $content, $subject));

                DB::commit(); 

                return back()->with('success', 'Tạo phản hồi thành công! 
                Chúng tôi sẽ sớm xử lý và trả lời cho bạn');
            }

            return back()->with('error', 'Đã xảy ra lỗi với hệ thống, không thể tạo phản hồi 
            ngay lúc này, bạn vui lòng quay lại sau!');
        } catch (\Exception $e) {
            DB::rollback(); 
            
            return back()->with('error', 'Đã xảy ra lỗi với hệ thống, không thể tạo phản hồi 
            ngay lúc này, bạn vui lòng quay lại sau!');
        }
    }
}
