<?php

namespace App\Http\Middleware;

use Closure;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $logged)
    {
        if($logged == 'false') {
            if(session('isLogin') ) {
                return redirect()->route('getStudentIndex');            
            } else {
                return $next($request);
            }
        } 
        if($logged == 'true') {
            if(!session('isLogin') ) {
                return redirect()->route('getStudentIndex');            
            } else {
                return $next($request);
            }
        } 
    }
}
