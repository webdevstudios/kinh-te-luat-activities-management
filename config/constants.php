<?php
return [

 'import_mode' => [
   'add_new' => [
      'value' => 0,
      'diskplay' => 'Thêm mới vào danh sách hiện tại'
    ],
   'reset' => [
      'value' => 1,
      'diskplay' => 'Xóa toàn bộ danh sách hiện tại và tạo mới'
    ],
  ],

  'print_status' => [
      'OPEN' => 0,
      'CANCEL' => 1,
      'DONE' => 2, 
      'ALL' => 3
  ],
  'feedback_status' => [
    'OPEN' => false,
    'CLOSE' => true,
    'ALL' => 'ALL'
  ]
];
?>